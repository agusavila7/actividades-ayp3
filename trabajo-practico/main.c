#include "listas.c"

int main(){   
    //*****************Utilizo el método para crear una lista*****************
    Nodo* lista = crearLista();


    //*****************Utilizo el método para insertar elementos de forma ordenada*****************
    insertarValorOrdenado(&lista, 7);
    insertarValorOrdenado(&lista, 2);
    insertarValorOrdenado(&lista, 15);
    insertarValorOrdenado(&lista, 4);
    insertarValorOrdenado(&lista, 10);
    insertarValorOrdenado(&lista, 5);
    insertarValorOrdenado(&lista, 8);
    insertarValorOrdenado(&lista, 4);
    insertarValorOrdenado(&lista, 19);
    insertarValorOrdenado(&lista, 20);


    //*****************Utilizo los métodos para imprimir y eliminar*****************
    printf("\n ----------Elminacion de datos----------\n");
    imprimirLista(lista);   //lista con el 5
    printf("\n Elimino nodo con valor 5: \n");
    eliminarDato(&lista,5); //elimino nodo con valor 5

    imprimirLista(lista);   // lista sin el 5 luego de "eliminarDato"


    //*****************Utilizo el método para obtener el largo de la lista*****************
    printf("\n----------Largo de la lista----------\n");
    printf("El largo de la lista es: %d", obtenerLargo(lista));


    //*****************Utilizo el método para buscar un elemento en la lista*****************
    printf("\n----------Busqueda de un elemento----------\n");
    
    Nodo* sal = buscar(lista,2);

    if(sal == NULL){
        printf("No se encontro el numero que buscabas");
    }else{
        printf("El dato esta en la direccion: %p", sal);
    }

    return 0;
}