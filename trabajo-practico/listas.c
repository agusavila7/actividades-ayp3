#include <stdio.h>
#include <malloc.h>
#include "entidades.h"

Nodo* crearLista(){
    Nodo *head = NULL;
    return head;
}

void insertarValorOrdenado(Nodo **lista, int valor){
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (*lista == NULL){
        *lista = nodoNuevo;
    } else {
        if(valor < (*lista)->valor){
            nodoNuevo->proximo = *lista;
            *lista = nodoNuevo;
        } else{
            Nodo *cursor = *lista;
            while(cursor->proximo != NULL && cursor->proximo->valor<valor) {
                cursor = cursor->proximo;
            }
            nodoNuevo->proximo=cursor->proximo;
            cursor->proximo = nodoNuevo;
        }
    }
}

void eliminarDato(Nodo **lista, int elementoABorrar){
    Nodo *actual = *lista;
    Nodo *anterior = NULL;
    while((actual!=NULL) && (actual->valor!=elementoABorrar)){
        //Actualizamos los valores de anterior y actual
        anterior=actual;
        actual = actual->proximo;
    }
    if(actual==NULL){
        //Significa que no se encontró el NODO con ese dato (o que la lista esta vacia)
        if(*lista == NULL){
            printf("La lista esta vacía\n");
        } else {printf("No se encontro el elemento en la lista\n");}
    }
    else{
        if(*lista == actual){
            *lista = actual->proximo;
        } //significa que queremos borrar el primer elemento
        else{
            anterior->proximo = actual ->proximo;
        }
        free(actual);
    }
}

Nodo* buscar(Nodo* lista, int elementoABuscar){
    Nodo* nodoAcutual = lista;
    while(nodoAcutual != NULL){
        if(nodoAcutual->valor == elementoABuscar){
            return nodoAcutual;
        }
        nodoAcutual = nodoAcutual->proximo;
    }
    return NULL;
}

void imprimirLista(Nodo* lista){
    Nodo* nodoAcutual = lista;

    while(nodoAcutual != NULL){
        printf("[%d],",nodoAcutual->valor);
        nodoAcutual = nodoAcutual->proximo;
    }
}

int obtenerLargo(Nodo *lista){
    Nodo* nodoAcutual = lista;
    int length = 0;
    while(nodoAcutual != NULL){
        length = length + 1;
        nodoAcutual = nodoAcutual->proximo;
    }
    return length;
}