#include <stdio.h>
#include "entidades.h"



void imprimirDatos(Persona persona){
    printf("\n--------DATOS DE LA PERSONA--------\n");
    printf("Nombre: %s \n", persona.nombre);
    printf("Apellido: %s \n", persona.apellido);
    printf("Edad: %i \n", persona.edad);
    printf("Estado civil: %s \n", persona.estadoCivil);
    printf("-----------------------------------\n");
}

int main(){

    int opcionMarcada;
    Persona persona_actual;

    do{
        printf("--Bienvenido al menu para crear su persona--\n");
        printf("1. Establecer nombre\n");
        printf("2. Establecer apellido\n");
        printf("3. Establecer edad\n");
        printf("4. Establecer estado civil\n");
        printf("5. Devolver datos\n");
        printf("6. Cancelar\n");
        printf("Ingrese una opcion: ");
        scanf("%d", &opcionMarcada);

        switch (opcionMarcada){
        case 1:
            printf("Ingrese un nombre: ");
            scanf("%s", &persona_actual.nombre);
            printf("\n");
            break;
        case 2:
            printf("Ingrese un apellido: ");
            scanf("%s", &persona_actual.apellido);
            printf("\n");
            break;
        case 3:
            printf("Ingrese una edad: ");
            scanf("%i", &persona_actual.edad);
            printf("\n");
            break;
        case 4:
            printf("Ingrese estado civil: ");
            scanf("%s", &persona_actual.estadoCivil);
            printf("\n");
            break;
        case 5:
            imprimirDatos(persona_actual);
            printf("\n");
            break;
        case 6:
            printf("Saliendo del menu...");
            break;
        default:
            printf("No se encontro la opcion indicada\n");
        }
    } while (opcionMarcada != 6);
        return 0;
    return 0;
}