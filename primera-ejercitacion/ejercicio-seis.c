#include <stdio.h>

int main(){
    int numero;

    printf("Ingrese un numero: ");
    scanf("%d", &numero);
    if (numero % 2 == 0){
        printf("Su numero es par.");
    }
    else{
        printf("Su numero NO es par.");
    }
    return 0;
}