#include <stdio.h>

int main(){
    int num_max = 0;
    int listado_num[5];
    listado_num[0] = 1;
    listado_num[1] = 9;
    listado_num[2] = 1;
    listado_num[3] = 5;
    listado_num[4] = 2;

    for (int i=0; i<sizeof(listado_num) / sizeof(int) ;i++){
        if(listado_num[i] > num_max){
            num_max = listado_num[i];
        }
    }
    printf("El numero maximo es: %d", num_max);
    return 0;
}
