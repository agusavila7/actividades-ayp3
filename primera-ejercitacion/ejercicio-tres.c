#include <stdio.h>

int main(){
    int listado_num[5];
    listado_num[0] = 15;
    listado_num[1] = 9;
    listado_num[2] = 21;
    listado_num[3] = 5;
    listado_num[4] = 2;

    int num_min = listado_num[0];

    for (int i=0; i<sizeof(listado_num) / sizeof(int) ;i++){
        if(listado_num[i] < num_min){
            num_min = listado_num[i];
        }
    }
    printf("El numero minimo es: %d", num_min);
    return 0;
}
